<?php if($this->Session->read('User') == NULL){  
    //if user is not logged in go to login page
    $this->Html->requestAction('employess/register');
}
?>
<h1>Welcome <?php echo $this->Session->read('User'); ?></h1>

<h3>Jobs worked on so far...</h3>

<table>
    <tr>
        <th>Job name</th>
        <th>Quote</th>
        <th>Total hours</th>
    </tr>
<?php foreach ($jobs as $job): ?>
<tr>
    <td><?php echo $job['SheetLog']['JobName']; ?></td>
    <td>$<?php echo $job['JobSheet']['Quote']?></td>
    <td><?php echo $job['0']['TotalHours']; ?></td>
</tr>
<?php endforeach; ?>
</table>