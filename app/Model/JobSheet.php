<?php

class JobSheet extends AppModel{
    
    public $useTable = 'JobSheets';
    
    public $validate = array(
        'Quote' => 'notEmpty',
        'Name' => array('minLength',3),
        'JobSheet_UserName' => array('between',7,7)
    );
    
    public $belongsTo = array(
        'Employee' => array(
            'foreignKey' => 'JobSheet_UserName'
        )
    );
}

?>
