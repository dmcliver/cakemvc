<?php

class SheetLog extends AppModel{
    public $useTable = 'SheetLogs';
    
    public $validate = array(
        'Hours' => 'numeric',
        'JobDate' => array(
            'rule' => array('date','dmy'),
            'required' => true,
            'message' => 'Please enter a valid date'
        )
    );
    
    public $belongsTo = array(
        'JobSheet' => array(
            'foreignKey' => 'SheetLog_JobName'
        )
    );
}

?>
