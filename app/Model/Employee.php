<?php
    class Employee extends AppModel{

         public $primaryKey = 'UserName';
        
        public $validate = array(
            'FirstName' => array(
                'rule' => array('minLength',3),
                'required' => true,
                'message' => 'First name must be greater than 3 characters long'
            ),
            'LastName' => array(
                'rule' => array('minLength',3),
                'required' => true,
                'message' => 'Second name must be greater than 3 characters long'
            ),
            'UserName' => array(
                'rule' => array('between',7,7),
                'required' => true,
                'message' => 'User name must be exactly 7 characters long'
            ),
            'Password' => array(
                'identicalFieldValues' => array(
                    'rule' => array('identicalFieldValues', 'Password_Confirmation' ),
                    'message' => 'Please re-enter your password twice so that the values match'
                )
            )
        );

        public function identicalFieldValues($field=array(),$compare_field=null){
            foreach( $field as $key => $value ){
                $modelField = $value;
                $confirmField = $this->data[$this->name][ $compare_field ];
                if($modelField !== $confirmField) {
                    return FALSE;
                } else {
                    continue;
                }
            }
            return TRUE;
        }
    }
