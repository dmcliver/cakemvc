<?php
    class EmployeesController extends AppController{
        
        public $helpers=array('Html','Form');
        
        public function index(){
            $this->set('employees',$this->Employee->find('all'));
        }

        public function register(){
            
            if ($this->request->is('post')) {
                
                $this->Employee->create();
                if ($this->Employee->save($this->request->data)) {
                    
                    $this->Session->setFlash('Registration successful.');
                    $this->redirect(array('action' => 'index'));
                } 
                else {
                    $this->Session->setFlash('Unable to add a new employee.');
                }
            }
        }

        public function login(){
            if($this->request->is('post')){
                
                $employee=$this->request->data['Employee'];
                $this->Employee->create();
                
                $result = $this->Employee->find(
                    'first', array(
                        'conditions'=>array(
                            'Employee.UserName'=>$employee['UserName'],
                            'Employee.Password'=>$employee['Password']
                        )
                    )
                );
                
                $this->loadModel('SheetLog');
                $jobs = $this->SheetLog->find(
                    'all', array(
                        'conditions' => array(
                            'JobSheet.JobSheet_UserName' => $result['Employee']['UserName']
                        ),
                        'fields' => array(
                            'SheetLog.SheetLog_JobName as JobName',
                            'JobSheet.Quote',
                            'SUM(SheetLog.Hours) as TotalHours'
                        ),
                        'group by' => 'SheetLog.SheetLog_JobName'
                    )
                );
                 
                if($result != NULL){
                    
                    $this->Session->write('User',$result['Employee']['FirstName'].' '.$result['Employee']['LastName']);
                    $this->set('jobs',$jobs);
                    $this->render('jobsheet');
                }
                else{
                    $this->Session->setFlash("User not found possibly due to incorrect username or password");
                }
            }
        }
    }
